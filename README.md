## Piction Client

Export data from Piction Digital Asset Management System via its API.

Forked from Imamuseum\PictionClient for PHP 7.x compatibility

### Installation

    composer require dallasmuseumofart/piction-client

### Composer Setup
```json
{
    "require": {
        "dallasmuseumofart/piction-client": "dev-master@dev"
    },
    "repositories": [
        {
            "type": "git",
            "url": "https://bitbucket.org/DallasMuseumArt/piction-client.git"
        }
    ]
}
```

### Environmental Variables Setup
Add Piction Variables to your environment

```
PICTION_ENDPOINT_URL=null
PICTION_IMAGE_URL=null
PICTION_USERNAME=null
PICTION_PASSWORD=null
PICTION_FORMAT=null
PICTION_SURL=null
```

### CHANGELOG

3.0.10 removed dotenv dependency, favoring getenv and putenv