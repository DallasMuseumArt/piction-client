<?php

namespace DMA\PictionClient;

use Exception;
use DMA\PictionClient\PictionHelpers;

class Piction
{
    /*
    Some remarks about Piction API and how this client works with it.
    - The REST API is a wrap of the existing SOAP webservice.
    - Calling methods in the API are done by GET
    - Piction has a particular URL structure to specify which method to call and which arguments to use.
      the following URL structure shows you what is the basic Piction REST API URL structure:
      http://piction.host.com/r/st/[method]/surl/[auth_token](/[param_name]/[param_value]/..)
    - The REST API can return return XML and JSON. By default it returns XML.
    */

    /**
     * @var helpers
     */
    private $helpers;

    public function __construct()
    {
        $this->helpers = new PictionHelpers();

        // These come out of environment variables
        $this->endpoint = getenv('PICTION_ENDPOINT_URL');
        $this->username = getenv('PICTION_USERNAME');
        $this->password = getenv('PICTION_PASSWORD');
        $this->format   = getenv('PICTION_FORMAT');
        $this->surl     = getenv('PICTION_SURL');

        $this->piction_method = "";
        $this->params   = [];

        // Check if surl is null and if so lets get a new one to store.
        if (is_null($this->surl) || $this->surl === "")
            $this->authenticate();
    }

    /* Autenticate to get new token for Piction access */
    public function authenticate()
    {
        $url = $this->endpoint .
            'piction_login/USERNAME/' . rawurlencode($this->username) .
            '/PASSWORD/' . rawurlencode($this->password) .
            '/' . $this->format . '/T';

        $response = $this->_curlCall($url);
        $response = $this->helpers->to_json($response);

        $this->surl = $response->SURL;
        putenv('PICTION_SURL='.$response->SURL);

        return $response->SURL;
    }

    /* Call a piction method and return response in the format requested. */
    public function call($piction_method, $params)
    {
        $this->piction_method = $piction_method;
        $this->params = $params;

        // We don't send parameters as ?key=value because Piction uses a specific URL structure for it.
        // The following method will create the url
        $url = $this->_buildURL();

        // Retreive first response
        $response = $this->_curlCall($url);
        $response = json_decode($response);

        // Check for pagination
        if (is_array($response->r) && count($response->r) > 0)
        {
            $pages = ceil($response->s->t / count($response->r));
            if ($pages > 1)
                for($i=1; $i<=$pages; $i++)
                {
                    $this->params['MAXROWS'] = count($response->r);
                    $this->params['START'] = (($i*100)+1);
    
                    $url = $this->_buildURL();
                    $next_response = json_decode($this->_curlCall($url));
                    $response->r = array_merge($response->r, $next_response->r);
                }
        }

        return $response;
    }

    /*
    Convert parameters to Piction URL structure
    http://piction.host.com/r/st/[method]/surl/[auth_token](/[param_name]/[param_value]/..)
    */
    private function _buildURL()
    {
        $url = "";

        // Format a pair key-value list
        foreach ($this->params as $key => $value) {
            $url .= strtoupper($key) . '/' . $this->_prepareValue($value) . '/';
        }

        // Add format to url if the correct parameters exist
        if (isset($this->format) && !is_null($this->format) && !isset($this->params['format'])) {
            $url .= $this->format . '/TRUE/';
        } elseif (isset($this->params['format'])) {
            $url .= $this->params['format'] . '/TRUE/';
        }

        // Build the url to request
        $url = $this->endpoint . $this->piction_method . '/surl/' . $this->surl . '(/' . $url . ')';

        return $url;
    }

    /* Convert values to Piction values */
    private function _prepareValue($value)
    {
        if ($value != "") {
            // Replace spaces with %20 to send params correctly
            $value = str_replace(' ', '%20', $value);
        }

        // Check if the value is a boolean and set to correct type of string
        if ($this->helpers->is_bool($value)) {
            if (($value != "") && ($value !== FALSE)) {
                $value = 'TRUE';
            } else {
                $value = 'FALSE';
            }
        }

        return $value;
    }

    /* Calls through curl to Piction */
    private function _curlCall($url)
    {
        // Get cURL resource
        $curl = curl_init();
        
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_URL => $url
        ));

        $response = curl_exec($curl);

        if (curl_errno($curl) > 0) {
            echo 'Curl error ' . curl_errno($curl) . ": " . curl_error($curl);
        }

        // Close request to clear up some resources
        curl_close($curl);

        // If surl validation fails, get a new surl and recycle the request
        if (strlen(strstr($response,'SURL failed validation')) > 0){
            $this->authenticate();
            $response = $this->call($this->piction_method, $this->params);
        }

        return $response;
    }


}