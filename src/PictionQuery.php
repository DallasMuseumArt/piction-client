<?php

namespace DMA\PictionClient;

use DMA\PictionClient\Piction;

class PictionQuery
{
    private $method;
    private $params;
    private $piction;

    public function __construct($method, $params=[])
    {
        $this->method = $method;
        $this->params = (is_array($params)) ? $params : [];
        $this->piction = new Piction();
    }

    public function addParam ($param_name, $param_value)
    {
        $this->params[$param_name] = $param_value;
    }

    public function params ()
    {
        return $this->params;
    }

    public function exec ()
    {
        return ($this->piction->call($this->method, $this->params));
    }
}
